# long-stream-bot
A bot for counting down time on a long stream and extending the length of the stream based on subs, bits, and channel points.

## Usage
1. First download a release from [here](https://gitlab.com/wwsean08/long-stream-bot/-/releases)
1. Extranct the zip file and open .long-stream-bot.yaml with notepad or a similar program, then update the username to your twitch username, update the initial time to what you want it to be, and any addon values (like the ammount of time to add for a sub) to whatever you want them to be.  Note that for channel points the title needs to exactly match the title of the channel point redemption on twitch.
1. BEFORE stream run long-stream-bot.exe as it need to get some credentials from twitch which you don't want exposed on stream, it will open twitch and request to authorize this app, once you authorize it, you can close the browser tab/window that was opened, and close the application as well.
1. Setup the text sources you want in OBS or similar streaming program, pointing to the files in question.
1. Start the application once it's time for your stream to begin and start the countdown.

## File breakdown
* long-stream-bot.exe - The actual application
* .long-stream-bot.yaml - The configuration file
* timer.txt - The countdown timer
* current-event.txt - The current event "allocated" to the time right now
* bits.txt - The number of bits events still to be worked off
* sub.txt - The number of sub events still to be worked off
* channel-points.txt - The number of channel point events still to be worked off