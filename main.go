package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/skratchdot/open-golang/open"
	"github.com/spf13/viper"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/topic"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/ttvclient"
)

type rootServer struct{}
type responseServer struct {
	wg *sync.WaitGroup
}

type Event struct {
	eventCategory string
	eventType     string
	eventDuration time.Duration
}

var eventQueueWriteLock *sync.Mutex
var countsWriteLock *sync.Mutex
var countdownWriteLock *sync.Mutex
var countdown time.Duration
var eventQueue []Event

func main() {
	viper.SetConfigName(".long-stream-bot")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
	countsWriteLock = new(sync.Mutex)
	countdownWriteLock = new(sync.Mutex)
	countdown = viper.GetDuration("initial-time")
	eventQueue = make([]Event, 0)

	if !viper.IsSet("stream.key") {
		wg := new(sync.WaitGroup)
		wg.Add(1)
		go webserver(wg)
		apiURL := "https://id.twitch.tv/oauth2/authorize?client_id=vx9vz8dkfalvni40k0by5rz10pij7m&redirect_uri=http://localhost:7777&response_type=token&scope=bits:read+channel:read:redemptions+channel_subscriptions+user:read:email"
		open.Run(apiURL)
		wg.Wait()
	}
	if !viper.IsSet("stream.id") {
		getUserID()
	}

	go tick()
	go eventQueueWorker()

	setupWebsockets()

	for {
		time.Sleep(time.Minute)
	}
}

func eventQueueWorker() {
	for {
		if len(eventQueue) == 0 {
			file, err := os.OpenFile("current-event.txt", os.O_RDWR|os.O_CREATE, 0666)
			if err != nil {
				println(err.Error())
			}
			file.WriteString("no events")
			file.Close()
			time.Sleep(time.Second)
			continue
		}
		event := eventQueue[0]
		eventQueue = eventQueue[:1]
		file, err := os.OpenFile("current-event.txt", os.O_RDWR|os.O_CREATE, 0666)
		if err != nil {
			println(err.Error())
			return
		}
		file.WriteString(event.eventType)
		file.Close()
		time.Sleep(event.eventDuration)
		updateCounts()
	}
}

func tick() {
	for {
		addTimeToCountdown(time.Second * -1)
		if countdown <= 0 {
			break
		}
		writeTimeToFile(countdown)
		time.Sleep(time.Second)
	}
	// Done
	// TODO: Revisit as the only way to start the countdown again is to restart
	writeTimeToFile(countdown)
}

func addTimeToCountdown(duration time.Duration) {
	countdownWriteLock.Lock()
	defer countdownWriteLock.Unlock()
	countdown += duration
}

func writeTimeToFile(countdown time.Duration) {
	file, err := os.OpenFile("timer.txt", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		println(err)
		return
	}
	defer file.Close()
	if countdown > 0 {
		file.WriteString(fmtDuration(countdown))
	} else {
		file.WriteString("Done!")
	}
}

func fmtDuration(d time.Duration) string {
	d = d.Round(time.Second)
	h := d / time.Hour
	d -= h * time.Hour
	m := d / time.Minute
	d -= m * time.Minute
	s := d / time.Second
	return fmt.Sprintf("%02d:%02d:%02d", h, m, s)
}

func setupWebsockets() {
	client, err := ttvclient.CreateClient(viper.GetString("stream.key"), ttvclient.TwitchPubSubHost)
	if err != nil {
		println(err.Error())
		return
	}
	client.SetBitsHandler(bitsHandler)
	client.SetSubscriptionsHandler(subHandler)
	client.SetChannelPointRedemptionHandler(channelPointsHandler)

	err = client.Subscribe([]topic.Topic{
		topic.Bits(viper.GetInt("stream.id")),
		topic.Subscriptions(viper.GetInt("stream.id")),
		topic.ChannelPointsRedemption(viper.GetInt("stream.id")),
	})

	if err != nil {
		println(err.Error())
	}
}

func updateCounts() {
	countsWriteLock.Lock()
	defer countsWriteLock.Unlock()
	eventMap := map[string]int{}
	for _, item := range eventQueue {
		eventMap[item.eventCategory] = eventMap[item.eventCategory] + 1
	}
	for key, value := range eventMap {
		file, err := os.OpenFile(fmt.Sprintf("%s.txt", key), os.O_RDWR|os.O_CREATE, 0666)
		if err != nil {
			println(err)
			continue
		}
		file.WriteString(strconv.FormatInt(int64(value), 10))
		file.Close()
	}
}

func bitsHandler(msg ttvclient.BitsMsg) {
	if !viper.GetBool("bits.enabled") {
		return
	}
	threshold := viper.GetInt("bits.threshold")
	if msg.Data.BitsUsed >= threshold {
		event := Event{
			eventCategory: "bits",
			eventDuration: viper.GetDuration("bits.time"),
		}
		if msg.IsAnonymous {
			event.eventType = "Anonymous Bits"
		} else {
			event.eventType = fmt.Sprintf("%s's Bits", msg.Data.UserName)
		}
		eventQueue = append(eventQueue, event)
		updateCounts()
	}
}

func subHandler(msg ttvclient.SubscriptionMsg) {
	if !viper.GetBool("subscriptions.enabled") {
		return
	}
	event := Event{
		eventCategory: "sub",
	}
	switch msg.SubPlan {
	case "Prime":
		event.eventDuration = viper.GetDuration("subscriptions.prime.time")
		event.eventType = fmt.Sprintf("%s's Prime Sub", msg.UserName)
		addTimeToCountdown(viper.GetDuration("subscriptions.prime.time"))
	case "1000":
		event.eventDuration = viper.GetDuration("subscriptions.tier1.time")
		event.eventType = fmt.Sprintf("%s's Sub", msg.UserName)
		if msg.IsGift {
			event.eventType = fmt.Sprintf("%s's Gifted Sub", msg.UserName)
		}
		addTimeToCountdown(viper.GetDuration("subscriptions.tier1.time"))
	case "2000":
		event.eventDuration = viper.GetDuration("subscriptions.tier2.time")
		event.eventType = fmt.Sprintf("%s's Tier 2 Sub", msg.UserName)
		if msg.IsGift {
			event.eventType = fmt.Sprintf("%s's Tier 2 Gifted Sub", msg.UserName)
		}
		addTimeToCountdown(viper.GetDuration("subscriptions.tier2.time"))
	case "3000":
		event.eventDuration = viper.GetDuration("subscriptions.tier3.time")
		event.eventType = fmt.Sprintf("%s's Tier 3 Sub", msg.UserName)
		if msg.IsGift {
			event.eventType = fmt.Sprintf("%s's Tier 3 Gifted Sub", msg.UserName)
		}
		addTimeToCountdown(viper.GetDuration("subscriptions.tier3.time"))
	}
	eventQueue = append(eventQueue, event)
	updateCounts()
}

func channelPointsHandler(msg ttvclient.ChannelPointRedemptionMsg) {
	if !viper.GetBool("channel-points.enabled") {
		return
	}
	if msg.Data.Redemption.Reward.Title == viper.GetString("channel-points.title") {
		addTimeToCountdown(viper.GetDuration("channel-points.time"))
		event := Event{
			eventCategory: "channel-points",
			eventDuration: viper.GetDuration("channel-points.time"),
			eventType:     fmt.Sprintf("%s's Channel Points", msg.Data.Redemption.User.Login),
		}
		eventQueue = append(eventQueue, event)
		updateCounts()
	}
}

func getUserID() {
	if !viper.IsSet("stream.username") {
		log.Fatal("the streamers username must be set in the configuration")
		os.Exit(-1)
	}

	client := http.DefaultClient
	req, err := http.NewRequest("GET", "https://api.twitch.tv/helix/users", nil)
	if err != nil {
		println(err.Error())
		return
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", viper.GetString("stream.key")))
	req.Header.Set("Client-Id", "vx9vz8dkfalvni40k0by5rz10pij7m")
	req.URL.Query().Set("login", viper.GetString("stream.username"))
	resp, err := client.Do(req)
	if err != nil {
		println(err.Error())
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		println(err.Error())
	}

	type Profile struct {
		ID          string `json:"id"`
		Login       string `json:"login"`
		DisplayName string `json:"display_name"`
	}
	type Data struct {
		Data []Profile `json:"data"`
	}

	data := &Data{}
	err = json.Unmarshal(body, data)
	if err != nil {
		println(err.Error())
		return
	}
	id, _ := strconv.Atoi(data.Data[0].ID)
	viper.Set("stream.id", id)
	viper.WriteConfig()
}

// launches a webserver to listen for responses twitch
func webserver(wg *sync.WaitGroup) {
	s := &rootServer{}
	r := &responseServer{wg}
	http.Handle("/", s)
	http.Handle("/key", r)
	http.ListenAndServe(":7777", nil)
}

func (s *rootServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(r.URL.Fragment))
	w.Write([]byte(`
	<html>
		<head>
			<title>Long Stream Bot Authentication</title>
		</head>
		<body>
			<script>
				const hash = document.location.hash;
				let ampIndex = hash.indexOf("&");
				let key = hash.substring(14,ampIndex);

				let xhttp = new XMLHttpRequest(); 
				xhttp.open("GET", "/key?key=".concat(key), false);
				xhttp.send(); 

				let tag = document.createElement("p");
				tag.innerText = "You may now close the window"
				let body = document.getElementsByTagName("body")[0];
				body.appendChild(tag);
			</script>
		</body>
	</html>	
	`))
}

func (server responseServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	viper.Set("stream.key", r.URL.Query().Get("key"))
	err := viper.WriteConfig()
	if err != nil {
		println(err.Error())
	}
	server.wg.Done()
	w.WriteHeader(http.StatusAccepted)
}
