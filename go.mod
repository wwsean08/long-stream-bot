module gitlab.com/wwsean08/long-stream-bot

go 1.15

require (
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/spf13/viper v1.7.1
	gitlab.com/wwsean08/go-ttv-pubsub v1.3.1
)
